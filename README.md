# gpu-performance-benchmarks
 These benchmarks are primarily taken from mlperf v.06 
 and containerized with rhel as a base image to run on Openshift.
 
The following mlperf v0.6 benchmarks are included in this repo
*      Maskrcnn
*      SSD (Single Stage Detector)
*      GNMT
*      Transformer

Miscellaneous benchmarks
*      ResNet
    
Smoke Tests are also included

*      Matrix Multiply 
   