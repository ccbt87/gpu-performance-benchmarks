To create the podman image for GNMT

$ podman build -f gnmt_dockerfile -t quay.io/myrepo/rhel_gnmt_smic >bld.OUT   2>&1  &

This will create a podman image that you can push to your quay.io repo "myrepo". 

You will pull his image in your pod spec yaml "gnmt-mlperf.yaml"